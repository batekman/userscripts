// ==UserScript==
// @name         Surfoscript
// @namespace    https://github.com/batekman/
// @version      0.0.0.0.3a-2
// @updateURL    https://raw.githubusercontent.com/batekman/userscripts/master/Surfoscript.user.js
// @description  try to take over the world!
// @author       Kirill Nevzgodov <kirill.nevzgodov@yandex.ru>
// @domain       surfory.com
// @match        https://surfory.com/*
// @include      https://surfory.com/*
// @grant        none
// ==/UserScript==

/*
 *   Surfoscript 0.0.0.0.3a «Вольный ветер»
 *
 *   Список изменений и идей:
 *   https://github.com/batekman/userscripts/Surfoscript-roadmap.russian.org
 *
 */

// Настройка осуществляется заменой «true» на «false» и наоборот,
// а также изменением чисел.

// Добавлять суффикс с названием сайта в заголовок страниц: Мои ответы — Surfory
var GENERAL_ADD_SURFORY_SUFFIX = true;
// Обработка вопросов только на страницах вопросов
var GENERAL_HIDER_ONLY_IN_QUESTIONS = false;
// Включить скрытие вопросов
var GENERAL_ENABLE_HIDER = true;

// Показывать текст скрытого вопроса
var QUESTIONS_HIDE_SHOW_TEXT = false;
// Показывать причину скрытия
var QUESTIONS_HIDE_SHOW_REASON = true;
// Частота обработки новых вопросов
var QUESTIONS_HIDE_UPDATE_INTERVAL = 500; // in milliseconds

// Скрывать вопросы…

// …содержащие ссылки: А ты уже установил браузер Амиго? https://amigo.mail.ru/
var QUESTIONS_HIDE_WITH_LINKS = true;
// …без вопросительного знака: Задавайте свои ответы
var QUESTIONS_HIDE_WITHOUT_QUESTION_MARK = true;
// …с несколькими подряд вопросительными знаками: Как тебя зовут???
var QUESTIONS_HIDE_MULTIPLE_QUESTION_MARKS = true;
// …с первой буквой в нижнем регистре: позволяете ли вы себе не нажимать шифт?
var QUESTIONS_HIDE_WITH_LOWERCASE_FIRST_LETTER = true;
// …с другими ошибками (пробелы перед вопросительными знаками и двоеточиями): У меня к вам такой вопрос : где скачать книги Розенталя ?
var QUESTIONS_HIDE_OTHER_ILLITERATES = true;
// …с упоминаниями «В Контакте»: А вы есть на ВК? Добавляйтесь!
var QUESTIONS_HIDE_VKONTAKTE = true;
// …содержащие многоточия: Жизнь — боль… А вы так не думаете?
var QUESTIONS_HIDE_ELLIPSIS = true;
// …содержащие длинные слова в верхнем регистре: Боитесь ли вы СТОМАТОЛОГОВ?
var QUESTIONS_HIDE_CAPS_WORDS = true;
// …содержащие слова с неправильным регистром: Кто живёт в xUSSR?
var QUESTIONS_HIDE_ALTERNATE_CAPS = true;
// …фамильярного тона: Как дела твои?
var QUESTIONS_HIDE_FAMILIARLY = true;
// …содержащие смайлы: Хотите анекдот?)))
var QUESTIONS_HIDE_WITH_SMILES = true;
// …про Surfory: Где здесь добавлять свою музыку?
var QUESTIONS_HIDE_ABOUT_HERE = true;
// …заключающиеся в арифметических расчётах: 3+4=?
var QUESTIONS_HIDE_WITH_ARITHMETICS = true;

// Минимальная длина вопроса, при которой он не будет скрыт. «-1» — отключение возможности.
var QUESTIONS_HIDE_WITH_LENGTH_LESS_THAN = -1;
// Максимальная длина вопроса, при которой он не будет скрыт. «-1» — отключение возможности.
var QUESTIONS_HIDE_WITH_LENGTH_MORE_THAN = -1;

/*
 * * * * * * * * * * * * * * * * * * * * * * *
 *                                           *
 *     Конец настроек.                       *
 *                                           *
 *     Далее идёт реализация скрипта.        *
 *                                           *
 * * * * * * * * * * * * * * * * * * * * * * *
 */


var QUESTIONS_HIDE = [];
var QUESTIONS_HIDE_DESCRIPTIONS = [];

if (QUESTIONS_HIDE_WITH_LENGTH_LESS_THAN >= 0) {
    QUESTIONS_HIDE.push('^.{0,' + QUESTIONS_HIDE_WITH_LENGTH_LESS_THAN + '}$');
    QUESTIONS_HIDE_DESCRIPTIONS.push('длина меньше, чем ' + QUESTIONS_HIDE_WITH_LENGTH_LESS_THAN + ' символов');
}

if (QUESTIONS_HIDE_WITH_LENGTH_MORE_THAN >= 0) {
    QUESTIONS_HIDE.push('^.{' + QUESTIONS_HIDE_WITH_LENGTH_MORE_THAN + ',}$');
    QUESTIONS_HIDE_DESCRIPTIONS.push('длина больше, чем ' + QUESTIONS_HIDE_WITH_LENGTH_MORE_THAN + ' символов');
}

if (QUESTIONS_HIDE_WITH_SMILES) {
    QUESTIONS_HIDE.push(/[\;:]\-?[\(\)D]/i);
    QUESTIONS_HIDE_DESCRIPTIONS.push('смайлы');
    QUESTIONS_HIDE.push(/^[^\(]*\)/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('смайлы');
    QUESTIONS_HIDE.push(/\([^\)]*$/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('смайлы');
}

if (QUESTIONS_HIDE_WITH_ARITHMETICS) {
    QUESTIONS_HIDE.push(/^[0-9\-\+\*\/÷\=\?\s]{3,}/i);
    QUESTIONS_HIDE_DESCRIPTIONS.push('арифметический пример');
}

if (QUESTIONS_HIDE_ABOUT_HERE) {
    QUESTIONS_HIDE.push(/(^|\s+)(тут|здесь|surfory|с[уеёи]рфор[аи]|с[её]рф|загружать музыку|(?:за)?с[её]рф(?:е|и(?:ли?|ть))?)(\s+|$)/i);
    QUESTIONS_HIDE_DESCRIPTIONS.push('вопрос про этот сайт');
}

if (QUESTIONS_HIDE_FAMILIARLY) {
    QUESTIONS_HIDE.push(/тво[ийя]\?\s*$/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('вопрос, заданный в фамильярном тоне');
}

if (QUESTIONS_HIDE_CAPS_WORDS) {
    QUESTIONS_HIDE.push(/[А-ЯA-Z]{5,}/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие длинных слов из букв исключительно в верхнем регистре');
}

if (QUESTIONS_HIDE_ALTERNATE_CAPS) {
    QUESTIONS_HIDE.push(/(?:^|\s*)[^\s]([а-яa-z]+[А-ЯA-Z]+)(?:$|\s*)+/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие слов с буквами различных регистров, кроме первой');
}

if (QUESTIONS_HIDE_WITH_LINKS) {
    QUESTIONS_HIDE.push(/https?:\/\//);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие гипертекстовых ссылок');
}

if (QUESTIONS_HIDE_WITHOUT_QUESTION_MARK) {
    QUESTIONS_HIDE.push(/^[^\?]*$/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('отсутствие знаков вопросов');
}

if (QUESTIONS_HIDE_WITH_LOWERCASE_FIRST_LETTER) {
    QUESTIONS_HIDE.push(/^\s*[a-zа-я]/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('вопрос начинается с буквы в нижнем регистре');
}

if (QUESTIONS_HIDE_OTHER_ILLITERATES) {
    QUESTIONS_HIDE.push(/\s+\?/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие пробельных символов перед вопросительными знаками');
    QUESTIONS_HIDE.push(/\s+:[^\(\)D\-]/i);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие пробельных символов перед двоеточиями');
}

if (QUESTIONS_HIDE_VKONTAKTE) {
    QUESTIONS_HIDE.push(/[^а-я]вк[^а-я]/i);
    QUESTIONS_HIDE_DESCRIPTIONS.push('упоминание социальной сети «В Контакте»');
}

if (QUESTIONS_HIDE_MULTIPLE_QUESTION_MARKS) {
    QUESTIONS_HIDE.push(/\?{2,}/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие нескольких знаков вопроса, идущих подряд');
}

if (QUESTIONS_HIDE_ELLIPSIS) {
    QUESTIONS_HIDE.push(/\.{2,}|…/);
    QUESTIONS_HIDE_DESCRIPTIONS.push('наличие многоточий');
}

var title = document.getElementsByTagName("title")[0];
var surf_suffix = " — Surfory";

if (window.location.href.match(/^https?:\/\/surfory\.com\/[0-9]+$/)) {
    var namereg = new RegExp(/(.*) — .*/);
    var username = namereg.exec(title.innerHTML)[1];
    title.innerHTML = username + surf_suffix;
}

if (window.location.href.match(/^https?:\/\/surfory\.com\/news$/)) {
    document.getElementsByTagName("title")[0].innerHTML = "Моя лента" + surf_suffix;
}

if (window.location.href.match(/^https?:\/\/surfory\.com\/news-/)) {
    var namereg = new RegExp(/(.*): .*/);
    var username = namereg.exec(document.getElementsByTagName("title")[0].innerHTML)[1];
    document.getElementsByTagName("title")[0].innerHTML = "Новость пользователя " + username + surf_suffix;
}

function hyphenToDash(s) {
    return s.replace(' - ', ' — ');
}

function addSurfSuffix(title) {
    if (!title.match(/ — Surfory$/)) {
        title = title + surf_suffix;
    }
    return title;
}

title.innerHTML = hyphenToDash(title.innerHTML);


if (window.location.href.match(/^https?:\/\/surfory\.com\/[0-9]+\/ask\/answers$/)) {
    title.innerHTML = "Мои ответы";
}

if (window.location.href.match(/^https?:\/\/surfory\.com\/[0-9]+\/ask\/questions$/)) {
    title.innerHTML = "Мои вопросы";
}

if (GENERAL_ADD_SURFORY_SUFFIX) {
    title.innerHTML = addSurfSuffix(title.innerHTML);
}

if (GENERAL_HIDER_ONLY_IN_QUESTIONS && !window.location.href.match(/^https?:\/\/surfory\.com\/(?:[0-9]+\/)?ask(?:\/|$)/)) {
    console.log("Surfoscript stopped");
    return;
}


function clearQuestions() {
    questions = document.getElementsByClassName("post-qa");

    for (var i = 0; i < questions.length; i++) {
        var question = questions[i];
        if (question.dataset.surfoparsed) {
            continue;
        }
        var text = question.getElementsByClassName("post__text")[0];
        var clean_text = text.innerHTML.replace('\n', ' ');
        clean_text = clean_text.replace(/\s{2,}/, ' ');
        for (var re_num = 0; re_num < QUESTIONS_HIDE.length; re_num++) {
            if (clean_text.match(QUESTIONS_HIDE[re_num])) {
                question.style.backgroundColor = '#f0f0f0';
                question.style.padding = '0';
                question.getElementsByClassName("post__comments")[0].style.display = "none";
                question.getElementsByClassName("post__info")[0].style.display = "none";
                question.getElementsByClassName("post__header")[0].style.padding = '0';
                question.getElementsByClassName("post__footer")[0].style.display = "none";
                question.getElementsByClassName("post-menu")[0].style.top = "3px";
                text.style.margin = '7px';
                text.style.padding = '0';
                text.style.minHeight = '0px';
                text.style.fontWeight = 'normal';
                text.style.color = "#999999";
                text.style.fontSize = '9pt';

                if (!QUESTIONS_HIDE_SHOW_TEXT) {
                    text.style.fontStyle = "italic";
                    text.style.fontSize = '9pt';
                    if (!QUESTIONS_HIDE_SHOW_REASON) {
                        text.innerHTML = "Вопрос скрыт";
                    } else {
                        text.innerHTML = "Вопрос скрыт. Причина: " + QUESTIONS_HIDE_DESCRIPTIONS[re_num];
                    }
                } else {
                    text.innerHTML = "[Причина скрытия: " + QUESTIONS_HIDE_DESCRIPTIONS[re_num] + '] ' + clean_text;
                }
                break;
            }
        }
        text.innerHTML = hyphenToDash(text.innerHTML);
        question.dataset.surfoparsed = true;
    }
}

window.setInterval(clearQuestions, QUESTIONS_HIDE_UPDATE_INTERVAL);
