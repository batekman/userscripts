// ==UserScript==
// @name        vk.com templates
// @include     *vk.com*im*
// @description Шаблоны для ВК
// @version     1.1
// @grant       none
// @copyright   2015, Maks
// @copyright   2016, batekman
// ==/UserScript==

function $_GET(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
}

function createButton(title) {
    var butt = document.createElement("div");
    butt.innerHTML = title;
    butt.style.marginTop = "2px";
    butt.style.paddingLeft = "27px";
    butt.style.paddingRight = "27px";
    butt.className = "flat_button im_send_cont fl_l";
    butt.id = "ButtonGoodId";
    butt.onclick = function() {
        document.getElementById("im_editable" + $_GET('sel')).innerHTML = title;
        document.getElementById('im_send').click();
    };
    document.getElementById("im_write_form").appendChild(butt);
}

function BGrefresh() {
    if (!document.getElementById("ButtonGoodId")) {
        var buttons = [
            'Хорошо',
            'ОК'
        ];

        for (i = 0; i < buttons.length; i++) {
            createButton(buttons[i]);
        }
    }
    setTimeout(BGrefresh, 300);
}
BGrefresh();
